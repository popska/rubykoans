# Triangle Project Code.

# Triangle analyzes the lengths of the sides of a triangle
# (represented by a, b and c) and returns the type of triangle.
#
# It returns:
#   :equilateral  if all sides are equal
#   :isosceles    if exactly 2 sides are equal
#   :scalene      if no sides are equal
#
# The tests for this method can be found in
#   about_triangle_project.rb
# and
#   about_triangle_project_2.rb
#
def triangle(s0, s1, s2)

  s = (s0 + s1 + s2) / 2.0

  ok = (s - s0) * (s - s1) * (s - s2)

  if s0 <= 0 || s1 <= 0 || s2 <= 0 || ok <= 0
    raise TriangleError, "Invalid sides entered"
  end

  if s0 == s1 && s1 == s2
    :equilateral
  elsif s0 == s1 || s1 == s2 || s0 == s2
    :isosceles
  else
    :scalene
  end
end



# Error class used in part 2.  No need to change this code.
class TriangleError < StandardError
end
